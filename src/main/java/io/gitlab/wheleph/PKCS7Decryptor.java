package io.gitlab.wheleph;

import org.bouncycastle.cms.CMSEnvelopedData;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.KeyTransRecipientInformation;
import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipient;
import org.bouncycastle.util.encoders.Base64;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Collection;

public class PKCS7Decryptor {

    private PrivateKey privateKey;

    public PKCS7Decryptor(String privateKeyStr) {
        try {
            byte[] privateKeyData = extractRawData(privateKeyStr, "PRIVATE KEY");
            PKCS8EncodedKeySpec kspec = new PKCS8EncodedKeySpec(privateKeyData);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            privateKey = kf.generatePrivate(kspec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException("Unable to parse private key");
        }
    }

    public String decrypt(String encryptedText) throws CMSException {
        byte[] data = extractRawData(encryptedText, "PKCS7");

        CMSEnvelopedData envelopedData = new CMSEnvelopedData(data);

        Collection<RecipientInformation> recipients = envelopedData.getRecipientInfos().getRecipients();
        KeyTransRecipientInformation recipientInfo = (KeyTransRecipientInformation) recipients.iterator().next();
        JceKeyTransRecipient recipient = new JceKeyTransEnvelopedRecipient(privateKey);

        return new String(recipientInfo.getContent(recipient));
    }

    private byte[] extractRawData(String text, String dataType) {
        return Base64.decode(text
                .replace(String.format("-----BEGIN %s-----", dataType), "")
                .replace(String.format("-----END %s-----", dataType), ""));
    }
}
