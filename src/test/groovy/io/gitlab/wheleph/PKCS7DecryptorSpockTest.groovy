package io.gitlab.wheleph

import org.apache.commons.io.IOUtils
import org.bouncycastle.util.encoders.Base64
import spock.lang.Specification

import java.nio.charset.Charset

class PKCS7DecryptorSpockTest extends Specification {
    def "decrypt"() {
        given:
        String privateKey = readResource("/private.key")
        String encryptedText = readResource("/encrypted.txt")
        String plainText = readResource("/plain.txt")

        PKCS7Decryptor decryptor = new PKCS7Decryptor(privateKey)

        when:
        def decryptedText = decryptor.decrypt(new String(Base64.decode(encryptedText)))

        then:
        decryptedText == plainText
    }

    private String readResource(String resourceName) {
        def resInputStream = getClass().getResourceAsStream(resourceName)
        return IOUtils.toString(resInputStream, Charset.defaultCharset()).trim()
    }
}
