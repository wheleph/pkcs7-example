package io.gitlab.wheleph;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class PKCS7DecryptorTest {

    @Test
    public void testDecrypt() throws IOException, CMSException {
        String privateKey = readResource("/private.key");
        String encryptedText = readResource("/encrypted.txt");
        String plainText = readResource("/plain.txt");

        PKCS7Decryptor decryptor = new PKCS7Decryptor(privateKey);

        String decryptedText = decryptor.decrypt(new String(Base64.decode(encryptedText)));

        assertEquals(plainText, decryptedText);
    }

    private String readResource(String resourceName) throws IOException {
        InputStream resInputStream = getClass().getResourceAsStream(resourceName);
        return IOUtils.toString(resInputStream, Charset.defaultCharset()).trim();
    }
}