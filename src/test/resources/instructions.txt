These files were generated in the following way:

echo -n "s3cr3t" > plain.txt
openssl req -x509 -nodes -newkey rsa:3072 -keyout private.key -out public.cer -subj "/C=UA"
cat plain.txt | openssl smime -encrypt -outform pem public.cer | base64 > encrypted.txt

To verify you can decrypt it:

cat encrypted.txt | base64 -D | openssl smime -decrypt -inform pem  -inkey private.key